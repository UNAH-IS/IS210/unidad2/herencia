﻿using System;

namespace Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Primera prueba: ClaseBase");

            ClaseBase c1 = new ClaseBase(10, 20, 30);
            
            c1.Metodo1();
            c1.Metodo2();

            Console.WriteLine("\nSegunda prueba: ClaseDerivada");

            ClaseDerivada c2 = new ClaseDerivada(100, 200, 300);

            c2.Metodo1();
            c2.Metodo2();

            Console.WriteLine("\nTercera prueba: ClaseBase-ClaseDerivada");

            ClaseBase c3 = new ClaseDerivada(1, 2, 3);
            c3.Metodo1();
            c3.Metodo2();

            Console.WriteLine("\nEs posible utilizar un metodo que no exista en la clase derivada");

            Console.WriteLine(c2.Sumar());

            ////////////////////////////////////////////////

            Console.WriteLine("\n\nEjemplo de Empleados");

            Empleado emp001 = new Empleado("Alfredo Diaz", "0801199012345", "0001");
            emp001.SueldoBase = 10000;

            Empleado emp002 = new EmpleadoComision("Veronica Ruiz", "0108199800123", "0003", 2500);
            emp002.SueldoBase = 8000;

            Console.WriteLine(emp001);
            Console.WriteLine(emp002);

            
            Console.ReadKey();
        }
    }
}
