﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Herencia
{
    class ClaseDerivada : ClaseBase
    {
        // Constructor en la clase derivada, en este caso utiliza los mismos argumentos que el del padre.
        public ClaseDerivada(int datoPrivado, int datoPublico, int datoProtegido) : 
            base(datoPrivado, datoPublico, datoProtegido) {}

        // El modificador {new} indica al compilador que use la implementación
        // de la clase derivada en lugar de la implementación de la clase
        // base. Cualquier código que no haga referencia a su clase sino a
        // la clase base, usará la implementación de la clase base.
        public new void Metodo1()
        {
            Console.WriteLine("Derivada - Metodo1");
        }

        // Con {override} se permite obtener la definicion mas nueva de un
        // metodo o propiedad. En la clase base dicha funcion es {virtual}
        public override void Metodo2()
        {
            Console.WriteLine("Derivada - Metodo2");
        }
    }
}
