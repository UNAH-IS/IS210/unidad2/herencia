﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Herencia
{
    class EmpleadoComision : Empleado
    { 
        // La unica forma de modificar esta propiedad es a traves del constructor
        public decimal Comision { get; }
        // Al dejar private el set se logra que solo el propio objeto pueda
        // modificar estos atributos a traves de un metodo
        public decimal VentasMensuales { get; private set; }
        public decimal MetaMensual { get; private set; }
        public EmpleadoComision(string nombre, string cedulaIdentidad, string codigoEmpleado, decimal comision):
            base(nombre, cedulaIdentidad, codigoEmpleado)
        {
            this.Comision = comision;
        }

        // Este metodo permite modificar dos propiedades con private set
        public void GetValoresMensuales(decimal metaMensual, decimal ventaMensual)
        {
            this.MetaMensual = metaMensual;
            this.VentasMensuales = ventaMensual;
        }

        // Se dejara override para permitir que la ultima definicion sea utilizada
        // si el tipo de Empleado es el que corresponde.
        public override decimal GetSalario()
        {
            decimal total;

            // Llama al metodo de la clase base para obtener el salario
            total = base.GetSalario();

            if (this.VentasMensuales >= this.MetaMensual)
            {
                total += this.Comision;
            }

            return total;
        }
    }
}
