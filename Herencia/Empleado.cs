﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Herencia
{
    class Empleado
    {
        // propiedades autoimplementadas
        public string Nombre { get; }
        public string CedulaIdentidad { get; }
        public string CodigoEmpleado { get; }
        public decimal SueldoBase { get; set; }

        // constructor
        public Empleado(string nombre, string cedulaIdentidad, string codigoEmpleado)
        {
            Nombre = nombre;
            CedulaIdentidad = cedulaIdentidad;
            CodigoEmpleado = codigoEmpleado;
        }

        public virtual decimal GetSalario()
        {
            return SueldoBase;
        }

        public override string ToString()
        {
            return $"Nombre: {Nombre} ({CodigoEmpleado}) gana base {this.GetSalario()}";
        }
    }
}
