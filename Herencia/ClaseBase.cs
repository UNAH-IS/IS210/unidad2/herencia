﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Herencia
{
    class ClaseBase
    {
        public int datoPublico;
        private int datoPrivado;
        protected int datoProtegido;
       

        public ClaseBase(int privado, int publico, int protegido)
        {
            this.datoPrivado = privado;
            this.datoProtegido = protegido;
            this.datoPublico = publico;
        }

        public int Sumar()
        {
            return this.datoPrivado + this.datoProtegido + this.datoPublico;
        }

        public void Metodo1()
        {
            Console.WriteLine("Base - Metodo1");
        }

        // Virtual permite que una funcion en una clase base sea modificada
        // cualquier clase que la herede, colocando override en la función a
        // modificar
        public virtual void Metodo2()
        {
            Console.WriteLine("Base - Metodo2");
        }


    }
}
